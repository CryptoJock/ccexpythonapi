# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Acknowledgements: https://github.com/ScriptProdigy/CryptsyPythonAPI
# was used as reference for this module

import urllib
import urllib2
import json
import time

class Ccex:
    def __init__(self, APIKey):
        self.APIKey = APIKey

    # send API request
    def api_query(self, method, req={}):
        if method == "getpairs":
            ret = urllib2.urlopen(urllib2.Request('https://c-cex.com/t/pairs.json'))
            return json.loads(ret.read())
        else:
            req['key'] = self.APIKey
            req['a'] = method
            post_data = urllib.urlencode(req)

            headers = {}
            ret = urllib2.urlopen(urllib2.Request('https://c-cex.com/t/r.html', post_data, headers))
            return json.loads(ret.read())

    # Inputs:
    #   None
    # Outputs:
    #   json formatted currency pairs
    def getPairs(self):
        return self.api_query('getpairs', {})

    # Inputs:
    #   None
    # Outputs:
    #   Users balance of each currency (json formatted)
    def getBalance(self):
        return self.api_query('getbalance', {})

    # Inputs:
    #   id - Market id
    # Outputs:
    #   Users balance of each currency (json formatted)
    def marketOrders(self, id):
        return self.api_query('orderlist', {'self' : 0, 'pair' : id})

